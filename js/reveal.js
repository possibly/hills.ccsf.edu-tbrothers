$(document).ready(function(){
    $('#reveal').hide();

    $('#revealPhone').click(function(){
        $('#reveal').html('<h1><center>925.998.9956</center></h1>');
        $('#reveal').slideToggle(400);
        return false;
    })

    $('#revealBitBucket').click(function(){
        $('#reveal').html('<a href="https://bitbucket.org/possiblylethal"><h1><center>My BitBucket account.</center></h1></a>');
        $('#reveal').slideToggle(400);
        return false;
    })

    $('#revealEmail').click(function(){
        $('#reveal').html('<h1 class="show-for-large-up"><center>tylerbrothers1@gmail.com</center></h1>');
        $('#reveal').append('<h2 class="show-for-medium-only"><center>tylerbrothers1@gmail.com</center></h2>');
        $('#reveal').append('<h3 class="show-for-small-only"><center>tylerbrothers1@gmail.com</center></h3>');
        $('#reveal').slideToggle(400);
        return false;
    })
})
